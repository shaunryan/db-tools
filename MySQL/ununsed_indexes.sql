SET GLOBAL userstat=1;

SELECT *, CONCAT(TABLE_SCHEMA, '.', TABLE_NAME) AS `table`
	, INDEX_NAME AS `index`
FROM INFORMATION_SCHEMA.INDEX_STATISTICS
WHERE ROWS_READ = 0
  AND TABLE_SCHEMA NOT IN ('mysql')


/* Set userstats back when no longer needed*/
SET GLOBAL userstat=0;

