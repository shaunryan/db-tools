SELECT table_schema AS "schema"
, table_name AS "table"
, ENGINE AS "engine"
, table_rows AS "rows"
, ROUND(data_length / ( 1024 * 1024 * 1024 ), 2) AS data_size_gb
, ROUND(index_length / ( 1024 * 1024 * 1024 ), 2) AS index_size_gb
, ROUND((data_length + index_length ) / ( 1024 * 1024 * 1024 ), 2) AS total_size_gb
, ROUND(index_length / data_length, 2) AS "index_fragmentation"
FROM information_schema.TABLES
where TABLE_SCHEMA NOT IN ('mysql', 'information_schema', 'performance_schema')
ORDER BY data_length + index_length  DESC


