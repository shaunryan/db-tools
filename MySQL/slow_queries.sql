SELECT ID, USER, HOST, DB, TIME_MS, STATE, INFO
FROM information_schema.processlist
WHERE Command NOT IN ('Sleep')
	AND TIME > 0
ORDER BY `TIME`, TIME_MS DESC
