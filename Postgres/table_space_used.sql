﻿SELECT 
    current_catalog AS database_name
  , schemaname || '.' || relname AS table_name
  , (SELECT COUNT(*) FROM pg_indexes I WHERE I.schemaname = T.schemaname AND I.tablename= T.relname) AS indexes
  , 1 as partitions
  , n_live_tup AS row_count
  , pg_table_size( ('"' || T.schemaname || '"."' || T.relname || '"') ) AS table_size
  , pg_indexes_size( ('"' || T.schemaname || '"."' || T.relname || '"') ) AS index_size
  , pg_total_relation_size( ('"' || T.schemaname || '"."' || T.relname || '"') ) AS total_size
 FROM pg_stat_user_tables T
ORDER BY n_live_tup DESC;
