﻿/*
   This file is part of db-tools.

    db-tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    db-tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_page_diagnose') IS NULL
	EXEC sys.sp_executesql N'CREATE PROCEDURE dbo.sp_page_diagnose AS PRINT ''Foo'''
GO
ALTER PROCEDURE dbo.sp_page_diagnose
AS
/*
  @Description: This procedure collects data about page latches and lists the tables affected by them
                While it is possible to use XEvents to achieve this - the method used here also works in
                SQL Server 2005
  @Author: Thomas Kejser
*/
SET NOCOUNT ON


DECLARE @S INT = 0
DECLARE @NumSamples INT = 1000000

CREATE TABLE #Samp 
(
	wait_type VARCHAR(255)
	, resource_description VARCHAR(255)
	, wait_time_ms BIGINT
)

/* Sample the page waits */
WHILE @S < @NumSamples BEGIN
	INSERT INTO #Samp (wait_type, resource_description, wait_time_ms)
	SELECT wait_type, resource_description , wait_duration_ms
	FROM sys.dm_os_waiting_tasks
	WHERE wait_type LIKE 'PAGE%'
	SET @S = @S + 1
END


/* Proces the input */
ALTER TABLE #Samp 
ADD schema_name SYSNAME
	, table_name SYSNAME
	, index_name SYSNAME
	
DECLARE @obj_id BIGINT
DECLARE @idx_id BIGINT	
/* Hold the output of DBCC PAGE */
DECLARE @page_out table (ParentObject NVARCHAR(255), [Object] NVARCHAR(255), Field NVARCHAR(255), VALUE NVARCHAR(255))	

DECLARE C CURSOR FOR SELECT resource_description
FROM #Samp 
OPEN C
DECLARE @resource_description VARCHAR(255)
	, @schema_name SYSNAME
	, @table_name SYSNAME
	, @index_name SYSNAME
FETCH NEXT FROM C INTO @resource_description

WHILE @@FETCH_STATUS = 0 BEGIN
	
	DECLARE @sql_page NVARCHAR(4000)
	SET @sql_page = '
		DBCC TRACEON(3604) WITH NO_INFOMSGS
		DBCC PAGE (' + REPLACE(@resource_description, ':', ',') + ',-1) WITH TABLERESULTS, NO_INFOMSGS'

	SELECT @obj_id = VALUE
	FROM @page_out
	WHERE Field = 'Metadata: ObjectId'
	
	SELECT @idx_id = VALUE
	FROM @page_out
	WHERE Field = 'Metadata: IndexId'
	
	DELETE FROM @page_out 
	INSERT @page_out
	EXECUTE (@sql_page)

	SELECT @schema_name = SCHEMA_NAME(schema_id)
		,  @table_name = O.name
		, @index_name = I.name
	FROM sys.indexes I
	JOIN sys.objects O
	  ON I.object_id = O.object_id
	WHERE I.object_id = @obj_id
	AND I.index_id = @idx_id

	UPDATE #Samp
	SET schema_name = @schema_name
		, table_name = @table_name
		, index_name = @index_name
	WHERE CURRENT OF C	
	FETCH NEXT FROM C INTO @resource_description
END

CLOSE C
DEALLOCATE C

SELECT schema_name + '.' + table_name AS table_name
	, index_name
	, SUM(wait_time_ms) AS wait_time_ms
FROM #Samp
GROUP BY schema_name + '.' + table_name
	, index_name
ORDER BY SUM(wait_time_ms) DESC
DROP TABLE #Samp 
GO
EXEC sp_MS_marksystemobject 'dbo.sp_page_diagnose'