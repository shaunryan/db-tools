﻿/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

	Author: Thomas Kejser
	Date: 2014-04-16
	Description: This script fixes up database that have bad settings
	  
*/


/* Fix bad owners (should always be sa for non system databases */
EXEC sp_MSforeachdb 'USE [?] IF DB_ID() > 4 EXEC sp_changedbowner ''sa'''

/* Fix Orphans when possible */
EXEC sp_MSforeachdb 'USE [?] 
CREATE TABLE #orphans (Username SYSNAME, UserSID VARBINARY(85))

INSERT INTO #orphans
EXEC sp_change_users_login ''Report''

DECLARE @sql NVARCHAR(4000)
DECLARE C CURSOR FOR 
SELECT ''EXEC sp_change_users_login ''''UPDATE_ONE'''', '''''' + Username
                + '''''', '''''' + Username + ''''''''  FROM #orphans

OPEN C
FETCH NEXT FROM C INTO @sql 
WHILE @@FETCH_STATUS = 0 BEGIN

	FETCH NEXT FROM C INTO @sql	
	EXECUTE (@sql)
END
CLOSE C
DEALLOCATE C
DROP TABLE #orphans 
'


/* Get rid of orphans that didnt find a parent */
EXEC sp_MSforeachdb 'USE [?] 
CREATE TABLE #orphans (Username SYSNAME, UserSID VARBINARY(85))

INSERT INTO #orphans
EXEC sp_change_users_login ''Report''

DECLARE @sql NVARCHAR(4000)
DECLARE C CURSOR FOR 
SELECT ''DROP USER '' + Username + '''' FROM #orphans

OPEN C
FETCH NEXT FROM C INTO @sql 
WHILE @@FETCH_STATUS = 0 BEGIN

	FETCH NEXT FROM C INTO @sql	
	EXECUTE (@sql)
END
CLOSE C
DEALLOCATE C
DROP TABLE #orphans 
'



