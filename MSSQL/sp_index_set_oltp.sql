USE [master]
GO
IF OBJECT_ID('dbo.sp_index_set_oltp') IS NOT NULL DROP PROCEDURE sp_index_set_oltp
GO
CREATE PROCEDURE dbo.sp_index_set_oltp
AS
/*
	Author: Thomas Kejser
	Description: Outputs the alter table statements that forces 
	             the database into row-lock only mode.
							 This is particularly useful for OLTP databases
*/
SET NOCOUNT ON
SELECT 'ALTER INDEX ' + i.name +  ' ON ' + s.name + '.' + t.name + ' SET (ALLOW_PAGE_LOCKS = OFF)'
FROM sys.indexes i
JOIN sys.tables t ON i.object_id = t.object_id
JOIN sys.schemas s ON t.schema_id = s.schema_id
WHERE i.[allow_page_locks] = 1
AND i.name IS NOT NULL /* No heaps */

SELECT 'ALTER TABLE ' + s.name +  '.' + t.name + ' SET (LOCK_ESCALATION = DISABLE)'
FROM sys.tables t 
JOIN sys.schemas s ON t.schema_id = s.schema_id
