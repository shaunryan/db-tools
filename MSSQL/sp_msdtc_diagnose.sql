﻿/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_msdtc_diagnose') IS NOT NULL DROP PROCEDURE dbo.sp_msdtc_diagnose
GO
CREATE PROCEDURE dbo.sp_msdtc_diagnose
AS
SELECT [host_name]
  , [program_name]
  , command
  , DB_NAME(R.database_id)
  , R.open_transaction_count
  , open_resultset_count
  , transaction_id
  , wait_time
  , R.wait_resource
  , WT.resource_description
  , ST.text
FROM sys.dm_exec_requests R
INNER JOIN sys.dm_exec_sessions S
  ON R.session_id = S.session_id
INNER JOIN sys.dm_os_waiting_tasks WT
  ON WT.session_id = R.session_id
INNER JOIN sys.dm_exec_connections C
  ON C.session_id = S.session_id
CROSS APPLY sys.dm_exec_sql_text(most_recent_sql_handle) ST
WHERE command LIKE 'TM%'
ORDER BY host_name
GO
EXEC sp_ms_marksystemobject 'dbo.sp_msdtc_diagnose' 
GO
EXEC dbo.sp_msdtc_diagnose

