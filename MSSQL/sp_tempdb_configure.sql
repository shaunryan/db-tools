﻿/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/
USE [master]
GO
IF OBJECT_ID('sp_tempdb_configure') IS NULL 
  EXEC sp_executesql N'CREATE PROCEDURE sp_tempdb_configure AS RETURN'
GO
ALTER PROCEDURE sp_tempdb_configure
  @root_folder NVARCHAR(4000) = 'D:\'
  , @log_size_gb INT = 10
  , @total_size_gb INT = 64
  , @num_files INT = 64
AS

DECLARE @db NVARCHAR(4000) = 'tempdb'

/* Move log file (rename and reconfigure) */
PRINT 'ALTER DATABASE [tempdb] MODIFY FILE ( NAME = N''templog'', FILENAME = ''' 
	+ @root_folder+ 'templog.ldf'', SIZE='+CAST(@log_size_gb AS VARCHAR)+'GB, FILEGROWTH=0)'


/* calculate required individual file sizes */
DECLARE @file_size_mb INT
SET @file_size_mb = 1024 * CAST(@total_size_gb AS FLOAT) / CAST(@num_files AS FLOAT)

PRINT 'ALTER DATABASE tempdb MODIFY FILE (NAME = tempdev, NEWNAME = tempdb001, FILENAME = '''
	+ @root_folder+ 'tempdb001.mdf'', SIZE='+CAST(@file_size_mb AS VARCHAR)+'MB, FILEGROWTH=0)'


DECLARE @f INT 
SET @f = 2 /* Start from 2, there is already a file in here */
WHILE @f <= @num_files BEGIN

	DECLARE @file_num CHAR(3)
	DECLARE @sql_file NVARCHAR(4000)
	SET @file_num = RIGHT('000' + CAST(@f AS VARCHAR), 3)

	SET @sql_file = 
'ALTER DATABASE <db> ADD FILE (
  NAME = ''tempdb<file_num>''
	, FILENAME = ''<RootDrive>tempdb<file_num>.ndf''
	, SIZE = <file_size_mb>MB
	, FILEGROWTH = 0%)
TO FILEGROUP [PRIMARY]'

	SET @sql_file = REPLACE(@sql_file, '<RootDrive>', @root_folder)
	SET @sql_file = REPLACE(@sql_file, '<file_num>', @file_num)
	SET @sql_file = REPLACE(@sql_file, '<file_size_mb>', @file_size_mb)
	SET @sql_file = REPLACE(@sql_file, '<db>', @db)

	PRINT @sql_file
		
	SET @f = @f + 1
END
GO
EXEC sp_tempdb_configure