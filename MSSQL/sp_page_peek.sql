﻿USE [master]
GO
IF OBJECT_ID ('dbo.sp_page_peek') IS NOT NULL BEGIN
	DROP PROCEDURE dbo.sp_page_peek
END
GO
CREATE PROCEDURE dbo.sp_page_peek
@resource_id NVARCHAR(50)	/* The resource ID of the page, format: db_id:file_id:page_id (as per wait_stats)*/
, @return_table_results BIT = 1 /* If set to 1, will return a table of results */
/* Returned information about the page */
, @page_type NVARCHAR(50) = NULL OUTPUT	
, @database_id BIGINT = NULL OUTPUT 
, @database_name SYSNAME = NULL OUTPUT
, @partition_id BIGINT = NULL OUTPUT
, @object_id BIGINT = NULL OUTPUT 
, @object_name SYSNAME = NULL OUTPUT 
, @index_id BIGINT = NULL OUTPUT 
, @index_name SYSNAME = NULL OUTPUT
, @free_bytes INT = NULL OUTPUT
, @num_rows INT = NULL OUTPUT
, @index_level INT = NULL OUTPUT
AS
BEGIN
SET NOCOUNT ON
	/* Transform the resource ID description to parameters for DBCC PAGE */
	/* TODO: Validate that this is indeed a correctly formatted resource ID */
	DECLARE @page_param NVARCHAR(255)
	SET @page_param = REPLACE(@resource_id, ':', ',')	

	/* Grab the database ID and name (for use later when we lookup index/object */
	SET @database_id = CAST(LEFT(@page_param, CHARINDEX(',',@page_param)-1)  AS INT)
	SET @database_name = DB_NAME(@database_id)

	/* Generate the DBCC PAGE command used for output and read data into table variable */
	DECLARE @sql_page NVARCHAR(4000)
	SET @sql_page = '
		DBCC TRACEON(3604) WITH NO_INFOMSGS
		DBCC PAGE (' + @page_param + ',1) WITH TABLERESULTS, NO_INFOMSGS'
	
	DECLARE @page_out table (ParentObject NVARCHAR(255), [Object] NVARCHAR(255), Field NVARCHAR(255), VALUE NVARCHAR(255))	
	INSERT @page_out
	EXECUTE (@sql_page)

	/* Translate enum to human readable value */
	SELECT @page_type = 
		CASE VALUE
		WHEN 1 THEN 'data'
		WHEN 2 THEN 'index'
		WHEN 3 THEN 'text'
		WHEN 4 THEN 'text'
		WHEN 8 then 'GAM'
		WHEN 9 THEN 'SGAM'
		WHEN 10 THEN 'IAM'
		WHEN 11 THEN 'PFS'
		WHEN 13 THEN 'Boot'
		WHEN 15 THEN 'FileHeader'
		WHEN 16 THEN 'DiffMap'
		WHEN 17 THEN 'ML Map Page'
	END
	FROM @page_out
	WHERE Field LIKE 'm_type'

	/* get the free bytes on the page */
	SELECT TOP 1 @free_bytes = VALUE FROM @page_out WHERE Field LIKE 'm_freeData%'

		
	/* If this page is part of table, pull out information about the table structure */
	IF @page_type IN ('data', 'index', 'text') BEGIN 
	
		SELECT TOP 1 @index_level = VALUE FROM @page_out WHERE Field LIKE 'm_level%'
		SELECT TOP 1 @num_rows = VALUE FROM @page_out WHERE Field LIKE 'm_slotCnt%'
		
	
		SELECT TOP 1 @index_id = VALUE FROM @page_out WHERE Field LIKE 'Metadata: IndexId%'
		SELECT TOP 1 @object_id = VALUE FROM @page_out WHERE Field LIKE 'Metadata: ObjectId%'
		SELECT TOP 1 @partition_id = VALUE FROM @page_out WHERE Field LIKE 'Metadata: PartitionId%'
		
		
		DECLARE @obj_out TABLE (index_name SYSNAME NULL, object_name SYSNAME NULL, schema_name SYSNAME NULL)
		DECLARE @sql_meta NVARCHAR(4000)
		SET @sql_meta = 
		'SELECT i.name AS index_name, o.name AS object_name, s.name AS schema_name
			FROM <db_name>.sys.indexes i
			JOIN <db_name>.sys.objects o ON i.object_id = o.object_id
			JOIN <db_name>.sys.schemas s ON o.schema_id = s.schema_id
			WHERE i.index_id = <index_id> AND i.object_id = <object_id>'
		SET @sql_meta = REPLACE(@sql_meta, '<db_name>', @database_name)
		SET @sql_meta = REPLACE(@sql_meta, '<index_id>', @index_id)
		SET @sql_meta = REPLACE(@sql_meta, '<object_id>', @object_id)
		
		INSERT @obj_out
		EXECUTE (@sql_meta)
		
		SELECT @object_name = schema_name + '.' + object_name
			, @index_name = index_name
		FROM @obj_out
	END
	
	/* Return results as a single row (we also return data as variable) */
	IF @return_table_results = 1 BEGIN
		SELECT @page_type AS page_type
			, @database_id AS database_id
			, @database_name AS database_name
			, @partition_id AS partition_id
			, @object_id AS object_id
			, @object_name AS object_name
			, @index_id AS index_id
			, @index_name AS index_name
			, @num_rows AS num_rows
			, @index_level AS index_level
	END
END;
GO
EXEC sp_ms_marksystemobject 'sp_page_peek' 


