USE [master]
GO
IF OBJECT_ID('zabbix.mirror_online_msg') IS NOT NULL DROP PROCEDURE zabbix.mirror_online_msg
GO
CREATE PROCEDURE zabbix.mirror_online_msg
AS
/*
	Author: Thomas Kejser
	Description: Lists the databases that have trouble with mirroring
*/ 

SET NOCOUNT ON

SELECT ISNULL('The following databases are in mirroring trouble: ' + dbo.GROUP_CONCAT(DB_NAME(database_id)), '')
FROM sys.database_mirroring
WHERE database_id > 4
  AND mirroring_role_desc = 'PRINCIPAL'
  AND 
 (
    mirroring_state_desc <> 'SYNCHRONIZED' /* Find non synch'ed mirrors*/
    OR mirroring_safety_level_desc <> 'FULL' /* Find async mirrors */
 )
GO
EXEC zabbix.mirror_online_msg

