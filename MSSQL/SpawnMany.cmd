REM Author: Thomas Kejser
REM Purpose: Spawns many copies of the same executable. Useful for running many things in parallel

@Echo off
ECHO Spawning %2 copies of %1

FOR /L %%i IN (1, 1, %2) DO (
	Echo Spawning thread %%i
	START "Worker%%i" /Min %1 %%i %2
)


