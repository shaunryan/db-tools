﻿USE [master]
GO
GO
IF OBJECT_ID('dbo.sp_protocol_connected') IS NULL
	EXEC sys.sp_executesql N'CREATE PROCEDURE dbo.sp_protocol_connected AS PRINT ''Foo'''
GO
ALTER PROCEDURE dbo.sp_protocol_connected
AS
/*
  @Description: Lists which protocols are currently logged into the server
                Good for making sure connections work right an duse the right protocol
*/
SELECT 
    C.session_id
  , S.host_name
  , C.net_packet_size
  , C.net_transport 
  , S.program_name
  , S.client_version
  , last_activity = CASE WHEN C.last_read > C.last_write THEN C.last_read ELSE C.last_write END
FROM sys.dm_exec_connections C
JOIN sys.dm_exec_sessions S
 ON C.session_id = S.session_id
WHERE is_user_process = 1 /* No System processors */
AND S.program_name NOT LIKE '%SQLAgent%' 
GO
EXEC sp_MS_marksystemobject 'dbo.sp_protocol_connected'
GO
EXEC dbo.sp_protocol_connected