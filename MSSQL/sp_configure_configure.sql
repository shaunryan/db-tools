﻿USE [master]
GO
/*
	Author:      Thomas Kejser
	Description: Reconfigures MSSQL to a more sane 
               default than the standard configuration.
               See: http://kejser.org/databases/default-configuration-of-sql-server-and-query-hints/

  

*/

/* Enable the things we need to see*/
EXEC sp_configure 'show advanced options', 1;
RECONFIGURE;

/* Most queries dont scale well beyond DOP 16*/
EXEC sp_configure 'max degree of parallelism', 16;
RECONFIGURE;

EXEC sp_configure 'max worker threads', 8192;
RECONFIGURE;

EXEC sp_configure 'backup compression default', 1;
RECONFIGURE;

EXEC sp_configure 'remote admin connections', 1;
RECONFIGURE;

/* Now, configure model */
ALTER DATABASE model SET RECOVERY SIMPLE;

/* Make the data file reasonably sized*/
ALTER DATABASE model MODIFY FILE (NAME = modeldev, FILEGROWTH = 0, SIZE = 100MB)

/* Give log log a good starting size */
ALTER DATABASE model MODIFY FILE (NAME = modellog, FILEGROWTH = 0, SIZE = 4GB)

/* Async stats are typicaly better than sync stats */
ALTER DATABASE model SET AUTO_UPDATE_STATISTICS_ASYNC ON

