/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_query_running') IS NOT NULL DROP PROCEDURE dbo.sp_query_running
GO
CREATE PROCEDURE dbo.sp_query_running
AS
SELECT R.last_wait_type, DB_NAME(H.dbid) AS database_name, H.text, COUNT(*) AS num_active 
FROM sys.dm_exec_connections C
LEFT JOIN sys.dm_exec_requests R ON R.session_id = C.session_id
CROSS APPLY sys.dm_exec_sql_text(most_recent_sql_handle) H
GROUP BY R.last_wait_type, DB_NAME(H.dbid), H.text
HAVING text IS NOT NULL
ORDER BY COUNT(*) DESC


SELECT WT.session_id
  , WT.wait_type
  , WT.wait_duration_ms
  , S.host_name
  , S.program_name
  , DB_NAME(R.database_id) AS database_name
  , H.[text]
  , P.query_plan
  , R.reads
  , R.logical_reads
  , R.writes
FROM sys.dm_os_waiting_tasks WT
INNER JOIN sys.dm_exec_sessions S
  ON S.session_id = WT.session_id
LEFT JOIN sys.dm_exec_requests R
  ON R.session_id = WT.session_id
OUTER APPLY sys.dm_exec_sql_text(sql_handle) H
OUTER APPLY sys.dm_exec_query_plan(plan_handle) P
WHERE S.is_user_process = 1
GO
EXEC sp_ms_marksystemobject 'dbo.sp_query_running' 
GO
EXEC dbo.sp_query_running
