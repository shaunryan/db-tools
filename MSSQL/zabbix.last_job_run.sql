USE [master]
GO
IF OBJECT_ID('zabbix.last_job_run') IS NOT NULL DROP PROCEDURE zabbix.last_job_run
GO

CREATE PROCEDURE zabbix.last_job_run
  @JobName VARCHAR(255) = NULL
AS
SET NOCOUNT ON
/*
	Author: Thomas Kejser
	Description: Queries the last succesful run of a job (returned as a UNIX timesamp)
*/ 

SELECT DATEDIFF(s, '1970-01-01', MAX(CONVERT(DATETIME, CONVERT(CHAR(8), run_date, 112) + ' ' 
    + STUFF(STUFF(RIGHT('000000' + CONVERT(VARCHAR(8), run_time), 6), 5, 0, ':'), 3, 0, ':'), 121)))
FROM msdb.dbo.sysjobhistory JH
INNER JOIN msdb.dbo.sysjobs J 
  ON JH.job_id = J.job_id
WHERE (J.name = @JobName OR @Jobname IS NULL)
  AND step_id = 0 /* step_id = 0 is the outcome of all steps. */
  AND run_status > 0 /* run_status = 0 is fail, any value above is success */

GO
EXEC zabbix.last_job_run


