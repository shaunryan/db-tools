﻿/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_database_space_used') IS NULL
	EXEC sys.sp_executesql N'CREATE PROCEDURE sp_database_space_used AS PRINT ''Foo'''
GO
ALTER PROCEDURE [dbo].[sp_database_space_used]
AS
SET NOCOUNT ON
/*
	Author: Thomas Kejser
	Date: 2014-04-16
	Description: Queries each database for the storage it has allocated 
	  Also provide details of active transactions that may hold up the log backups
*/

/* Get database space */
CREATE TABLE #spaceused 
(
	db_name SYSNAME
	, size BIGINT
	, SpaceUsed BIGINT	
	, file_id BIGINT
)

EXEC sp_MSforeachdb '
	USE [?]; 
	INSERT #spaceused 
	SELECT name, size, FILEPROPERTY(name, ''SpaceUsed''), file_id
FROM sys.database_files'

/* Get info about the transaction logs */
CREATE TABLE #logused
(
	db_name SYSNAME
    , size BIGINT
    , SpaceUsed FLOAT
	, [status] BIGINT
)

INSERT #logused
EXEC sys.sp_executesql N'DBCC SQLPERF(logspace)'

SELECT name
	, log_allocated_mb = MAX(log_allocated_kb  / 1024) 
	, log_used_mb = MAX(log_used_kb  / 1024) 
	, log_free_mb = MAX((log_allocated_kb - log_used_kb)  / 1024) 
	, last_log_backup = (SELECT CAST(MAX(backup_finish_date) AS SMALLDATETIME) 
	   FROM msdb..backupset BS WITH (NOLOCK) 
	   WHERE BS.database_name = G.name 
	     AND BS.[type] = 'L') 
	, oldest_transaction = CAST(MIN(database_transaction_begin_time) AS SMALLDATETIME) 
	, uncommitted_log_mb = SUM(database_transaction_log_bytes_reserved + database_transaction_log_bytes_reserved_system) / 1024 / 1024 
	, data_allocated_mb = MAX(data_allocated_kb / 1024) 
	, data_used_mb = MAX(data_used_kb / 1024) 
	, data_free_mb = MAX((data_allocated_kb - data_used_kb) / 1024) 
	, last_data_backup = (SELECT CAST(MAX(backup_finish_date) AS SMALLDATETIME) FROM msdb..backupset BS WITH (NOLOCK) 
	   WHERE BS.database_name = G.name 
	     AND BS.[type] = 'D')  
FROM 
  (
	SELECT 
	  D.name
	, SUM(CASE F.type_desc WHEN 'LOG' THEN SU.size * 8 ELSE 0 END) log_allocated_kb
	, SUM(CASE F.type_desc WHEN 'LOG' THEN CAST(SU.size * 8 * LU.SpaceUsed / 100.0 AS BIGINT) ELSE 0 END) log_used_kb
	, SUM(CASE F.type_desc WHEN 'LOG' THEN CAST(0 AS BIGINT) ELSE CAST(SU.size * 8 AS BIGINT) END) AS data_allocated_kb
	, SUM(CASE F.type_desc WHEN 'LOG' THEN 0 ELSE CAST(SU.SpaceUsed * 8 AS BIGINT) END) AS data_used_kb
	FROM sys.master_files F  WITH (NOLOCK)
	INNER JOIN sys.databases D  WITH (NOLOCK)
	  ON F.database_id = D.database_id
	INNER JOIN #spaceused SU 
	  ON SU.FILE_ID = F.file_id
	  AND SU.db_name = F.name
	LEFT JOIN #logused LU 
	  ON D.name = LU.db_name AND F.type_desc = 'LOG'
	GROUP BY D.name
  ) G
LEFT JOIN sys.dm_tran_database_transactions DBT
  ON DBT.database_id = DB_ID(G.name)
  AND database_transaction_type != 2 /* We dont care about read-only tx */
  AND database_transaction_state NOT IN (10, 11) /* Comitted and rolled back dont matter */
GROUP BY name

DROP TABLE #spaceused 
DROP TABLE #logused

GO
EXEC sp_MS_marksystemobject 'dbo.sp_database_space_used'
GO
EXEC dbo.sp_database_space_used
