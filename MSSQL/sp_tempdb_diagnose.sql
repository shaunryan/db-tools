﻿/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_tempdb_diagnose') IS NOT NULL DROP PROCEDURE dbo.sp_tempdb_diagnose
GO
CREATE PROCEDURE dbo.sp_tempdb_diagnose
AS
/*
	Author: Thomas Kejser
	Description: Lists all objects in tempdb. Will also output issues with tempdb configuration
*/
SET NOCOUNT ON
SELECT
 s.session_id
 , s.status
 , DB_NAME(r.database_id) AS source_database
 , DB_NAME(dt.database_id) AS work_database
 , mg.requested_memory_kb
 , mg.granted_memory_kb 
 , mg.used_memory_kb
 , mg.max_used_memory_kb
 , su.internal_objects_alloc_page_count * 8 AS internal_objects_alloc_kb
 , su.internal_objects_dealloc_page_count * 8 AS internal_objects_dealloc_kb
 , su.user_objects_alloc_page_count * 8 AS user_objects_alloc_kb
 , su.user_objects_dealloc_page_count * 8 AS user_object_dealloc_kb
 , t.text AS query_text
 , s.[host_name]
 , s.[program_name]
FROM sys.dm_exec_sessions s
INNER JOIN sys.dm_db_session_space_usage su 
   ON s.session_id = su.session_id 
   AND su.database_id = DB_ID('tempdb') 
INNER JOIN sys.dm_exec_connections c 
   ON s.session_id = c.most_recent_session_id 
LEFT OUTER JOIN sys.dm_exec_requests r 
   ON r.session_id = s.session_id 
LEFT OUTER JOIN (
    SELECT
      session_id,
      database_id
     FROM sys.dm_tran_session_transactions t
     INNER JOIN sys.dm_tran_database_transactions dt
       ON t.transaction_id = dt.transaction_id  
     WHERE dt.database_id = DB_ID('tempdb') 
     GROUP BY  session_id,  database_id
   ) dt
  ON s.session_id = dt.session_id
OUTER APPLY sys.dm_exec_sql_text(COALESCE(r.sql_handle, c.most_recent_sql_handle)) t
LEFT OUTER JOIN sys.dm_exec_query_memory_grants mg 
  ON s.session_id = mg.session_id 
WHERE s.session_id <> @@SPID
ORDER BY s.session_id;

DECLARE @NumCores INT
SELECT @NumCores = cpu_count / hyperthread_ratio 
FROM sys.dm_os_sys_info

DECLARE @NumFiles INT
SELECT @NumFiles = COUNT(*) 
FROM sys.master_files
WHERE database_id = DB_ID('tempdb')
AND type_desc = 'ROWS'


IF @NumFiles < @NumCores BEGIN
	RAISERROR('tempdb has only %i files, but the machine has %i cores', 16, 1, @NumFiles, @NumCores)
END
GO
EXEC sp_ms_marksystemobject 'dbo.sp_tempdb_diagnose' 