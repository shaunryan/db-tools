﻿USE [master]
GO
IF OBJECT_ID('dbo.sp_index_oltp_mode') IS NULL
  EXEC sp_executesql N'CREATE PROCEDURE dbo.sp_index_oltp_mode AS RETURN'
GO
ALTER PROCEDURE dbo.sp_index_oltp_mode
AS
/*
  Author: Thomas Kejser
  Date: 2014-05-29
  Description: Prints out statement that disable old school index settings for all tables
    in the current database
*/
SELECT 'ALTER INDEX ' + i.name +  ' ON ' + s.name + '.' + t.name + ' SET (ALLOW_PAGE_LOCKS = OFF)'
FROM sys.indexes i
JOIN sys.tables t ON i.object_id = t.object_id
JOIN sys.schemas s ON t.schema_id = s.schema_id
WHERE i.[allow_page_locks] = 1
AND i.name IS NOT NULL /* No heaps */

SELECT 'ALTER TABLE ' + s.name +  '.' + t.name + ' SET (LOCK_ESCALATION = DISABLE)'
FROM sys.tables t 
JOIN sys.schemas s ON t.schema_id = s.schema_id
GO
EXEC sp_ms_marksystemobject 'dbo.sp_index_oltp_mode' 