﻿/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
IF OBJECT_ID('dbo.sp_mirror_diagnose') IS NOT NULL DROP PROCEDURE dbo.sp_mirror_diagnose
GO
CREATE PROCEDURE dbo.sp_mirror_diagnose
AS
BEGIN
  SELECT WT.wait_type
  , ST.text
  , SUM(WT.wait_duration_ms) AS duration
  , COUNT(*) as waits
  FROM sys.dm_os_waiting_tasks WT
  INNER JOIN sys.dm_exec_requests ER
    ON ER.session_id = WT.session_id
  CROSS APPLY sys.dm_exec_sql_text(ER.sql_handle) ST
  WHERE 
  (WT.wait_type LIKE 'DBMIRROR%' 
	  OR WT.wait_type LIKE 'THREAD%'
	  OR WT.wait_type LIKE 'WRITELOG%')
  AND WT.wait_type NOT IN ('DBMIRRORING_CMD')
  GROUP BY WT.wait_type
  , ST.text
  ORDER BY SUM(WT.wait_duration_ms) DESC
END