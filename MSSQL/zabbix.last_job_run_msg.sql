USE [master]
GO
IF OBJECT_ID('zabbix.last_job_run_msg') IS NOT NULL DROP PROCEDURE zabbix.last_job_run_msg
GO

CREATE PROCEDURE zabbix.last_job_run_msg
  @JobName VARCHAR(255) = NULL
AS
SET NOCOUNT ON
/*
	Author: Thomas Kejser
	Description: Queries the status of the last run job
*/ 

SELECT TOP 1 CASE run_status WHEN 0 THEN [message] ELSE '' END AS [status]
FROM msdb.dbo.sysjobhistory JH
INNER JOIN msdb.dbo.sysjobs J 
  ON JH.job_id = J.job_id
WHERE (J.name = @JobName OR @Jobname IS NULL)
  AND step_id = 0 /* step_id = 0 is the outcome of all steps. */
ORDER BY run_date DESC
GO
EXEC zabbix.last_job_run_msg
