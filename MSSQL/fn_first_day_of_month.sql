/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/
IF OBJECT_ID('dbo.fn_first_day_of_month') IS NULL 
	EXEC sp_executesql N'CREATE FUNCTION dbo.fn_first_day_of_month () RETURNS INT AS RETURN 42'
GO
ALTER FUNCTION dbo.fn_first_day_of_month (
  @date DATETIME
)
RETURNS VARCHAR(MAX)
AS 
BEGIN

  RETURN DATEADD(month, DATEDIFF(month, 0, @date), 0)
END
GO
GO
PRINT dbo.fn_first_day_of_month (GETUTCDATE())
