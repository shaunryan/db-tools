/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/
IF OBJECT_ID('dbo.fn_zero_pad') IS NOT NULL DROP FUNCTION dbo.fn_zero_pad
GO
CREATE FUNCTION dbo.fn_zero_pad (
  @number INT
  , @pad_length INT
)
RETURNS VARCHAR(MAX)
AS 
BEGIN

  RETURN RIGHT(REPLICATE('0', @pad_length) + CAST(@number AS VARCHAR(50)), @pad_length)
END
GO
PRINT dbo.fn_zero_pad (2, 4)
