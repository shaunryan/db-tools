﻿/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_volume_space_used') IS NULL
	EXEC sys.sp_executesql N'CREATE PROCEDURE dbo.sp_volume_space_used AS PRINT ''Foo'''
GO
ALTER PROCEDURE sp_volume_space_used
AS
CREATE TABLE #spaceused 
(
	db_name SYSNAME
	, size BIGINT
	, SpaceUsed BIGINT	
	, file_id BIGINT
)

EXEC sp_MSforeachdb '
	USE [?]; 
	INSERT #spaceused 
	SELECT name, size, FILEPROPERTY(name, ''SpaceUsed''), file_id
FROM sys.database_files'



SELECT volume_mount_point
	, volume_size / POWER(1024,2) AS volume_size_mb
	, (volume_size - volume_free) / POWER(1024,2) AS volume_used_mb
	, volume_free / POWER(1024,2) AS volume_free_mb
	, log_used_kb / 1024 AS log_used_mb
	, data_allocated_kb / 1024 AS data_allocated_mb
	, data_used_kb / 1024 AS data_used_mb
	, (data_allocated_kb - data_used_kb) / 1024 AS data_free_mb
	, (data_allocated_kb - data_used_kb) / 1024 + volume_free / POWER(1024,2) AS total_free_mb
FROM (
	SELECT volume_mount_point
	, MAX(total_bytes)  AS volume_size
	, MAX(available_bytes)  AS volume_free
	, SUM(CASE F.type_desc WHEN 'LOG' THEN SU.size ELSE 0 END) log_used_kb
	, SUM(CASE F.type_desc WHEN 'LOG' THEN CAST(0 AS BIGINT) ELSE CAST(SU.size AS BIGINT) * 8 END) AS data_allocated_kb
	, SUM(CASE F.type_desc WHEN 'LOG' THEN 0 ELSE CAST(SU.SpaceUsed * 8 AS BIGINT) END) AS data_used_kb
	FROM sys.master_files F
	CROSS APPLY sys.dm_os_volume_stats(F.database_id, F.file_id) VS
	INNER JOIN #spaceused SU 
	  ON SU.FILE_ID = F.file_id
	  AND SU.db_name = F.name
	GROUP BY volume_mount_point
) G

DROP TABLE #spaceused 

GO
EXEC sp_MS_marksystemobject 'dbo.sp_volume_space_used'
GO
EXEC dbo.sp_volume_space_used
