﻿/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_query_usage') IS NULL 
  EXEC sp_executesql N'CREATE PROCEDURE dbo.sp_query_usage AS RETURN'
GO
ALTER PROCEDURE dbo.sp_query_usage
	@db_name SYSNAME /* The database to query for */
AS
SELECT T AS query_text, SUM(execution_count) AS execution_count
FROM 
(
  SELECT 
    LTRIM(REPLACE(SUBSTRING(ST.text, CHARINDEX('CREATE PR',ST.text), CHARINDEX('AS',ST.text, CHARINDEX('CREATE PR',ST.text)) - CHARINDEX('CREATE PR',ST.text))
      , CHAR(13) + CHAR(10), ' ')) AS T
    , execution_count
  FROM sys.dm_exec_query_stats QS
  OUTER APPLY sys.dm_exec_sql_text(QS.sql_handle) ST
  WHERE DB_NAME(dbid) = @db_name
    AND CHARINDEX('CREATE PR',ST.text) > 0
  UNION ALL
  SELECT LTRIM(REPLACE(REPLACE(LEFT(ST.text, 200), CHAR(10), ' '), CHAR(13), '')) AS T, execution_count 
  FROM sys.dm_exec_query_stats QS
  OUTER APPLY sys.dm_exec_sql_text(QS.sql_handle) ST
  WHERE dbid IS NULL
    AND CHARINDEX(@db_name,ST.text) > 0
) U
GROUP BY T
ORDER BY SUM(execution_count) DESC
GO

