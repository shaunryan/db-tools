USE [master]
GO
IF OBJECT_ID('zabbix.mirror_online') IS NOT NULL DROP PROCEDURE zabbix.mirror_online
GO
CREATE PROCEDURE zabbix.mirror_online
AS
/*
	Author: Thomas Kejser
	Description: Returns a value > 1 if a database has trouble with mirroring
*/ 
SET NOCOUNT ON

SELECT CASE WHEN COUNT(*) IS NULL THEN 0 ELSE 1 END
FROM sys.database_mirroring
WHERE database_id > 4
  AND mirroring_role_desc = 'PRINCIPAL'
  AND 
 (
    mirroring_state_desc <> 'SYNCHRONIZED' /* Find non synch'ed mirrors*/
    OR mirroring_safety_level_desc <> 'FULL' /* Find async mirrors */
 )
GO
EXEC zabbix.mirror_online


