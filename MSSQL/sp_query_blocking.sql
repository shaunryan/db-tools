/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_query_blockers') IS NOT NULL DROP PROCEDURE dbo.sp_query_blockers
GO
CREATE PROCEDURE dbo.sp_query_blockers
AS
SET NOCOUNT ON

SELECT
	db.name database_name,
	tl.request_session_id,
	wt.blocking_session_id,
	OBJECT_NAME(p.OBJECT_ID) blocking_object,
	tl.resource_type,
	h1.TEXT AS requesting_query,
	h2.TEXT AS blocking_query,
	tl.request_mode
FROM sys.dm_tran_locks AS tl
INNER JOIN sys.databases db ON db.database_id = tl.resource_database_id
INNER JOIN sys.dm_os_waiting_tasks AS wt ON tl.lock_owner_address = wt.resource_address
INNER JOIN sys.partitions AS p ON p.hobt_id = tl.resource_associated_entity_id
INNER JOIN sys.dm_exec_connections ec1 ON ec1.session_id = tl.request_session_id
INNER JOIN sys.dm_exec_connections ec2 ON ec2.session_id = wt.blocking_session_id
CROSS APPLY sys.dm_exec_sql_text(ec1.most_recent_sql_handle) AS h1
CROSS APPLY sys.dm_exec_sql_text(ec2.most_recent_sql_handle) AS h2
GO
EXEC sp_ms_marksystemobject 'sp_query_blockers' 
GO
EXEC dbo.sp_query_blockers
