/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_query_diagnose') IS NOT NULL DROP PROCEDURE dbo.sp_query_diagnose
GO
CREATE PROCEDURE sp_query_diagnose
  @WaitDelay varchar(16)='00:00:05.000'
AS 	
/*
	Author: Thomas kejser
	Date: 2014-04-16
	Description: Provides detailed information on queries that have been run in the last @WaitDelay period
*/
SET NOCOUNT ON
IF OBJECT_ID('tempdb.dbo.#__ExecQStats_1') IS NOT NULL DROP TABLE #__ExecQStats_1
IF OBJECT_ID('tempdb.dbo.#__ExecQStats_2') IS NOT NULL DROP TABLE #__ExecQStats_2


SELECT 
   qs.total_elapsed_time
  ,qs.execution_count
  ,qs.total_worker_time
  ,CAST(qs.total_elapsed_time / 1000 AS FLOAT) / CAST(qs.execution_count as FLOAT) as avg_cpu_time_ms
  ,qs.statement_start_offset
  ,qs.statement_end_offset
  ,qs.query_hash /* May have to exclude this column on 2008 and 2005 */
  ,qs.sql_handle
  ,qs.plan_handle 
INTO #__ExecQStats_1
FROM sys.dm_exec_query_stats qs
WHERE qs.execution_count>0


WAITFOR DELAY @WaitDelay;



SELECT 
  qs.total_elapsed_time
  ,qs.execution_count
  ,qs.total_worker_time
  ,CAST(qs.total_elapsed_time / 1000 AS FLOAT) / CAST(qs.execution_count as FLOAT) as avg_cpu_time_ms
  ,qs.statement_start_offset
  ,qs.statement_end_offset
  ,qs.query_hash
  ,qs.sql_handle
  ,qs.plan_handle 
INTO #__ExecQStats_2
FROM sys.dm_exec_query_stats qs
WHERE qs.execution_count>0
ORDER BY total_elapsed_time DESC


select 
	CAST(CAST(qs.total_worker_time / 1000 as FLOAT) / CAST(qs.execution_count AS FLOAT) AS NUMERIC(15,3)) as avg_cpu_time_ms
	, total_worker_time AS total_cpu_time
	, CAST(CAST(qs.total_elapsed_time / 1000 as FLOAT) / CAST(qs.execution_count AS FLOAT) AS NUMERIC(15,3)) as avg_elapsed_time_ms
	, total_elapsed_time
	, total_worker_time
	, CAST(100 * CAST((total_elapsed_time - total_worker_time) AS FLOAT) / total_elapsed_time AS NUMERIC(5,2)) AS total_idle_time_percent
	, CASE WHEN CAST(qp.query_plan AS XML).value('declare namespace p="http://schemas.microsoft.com/sqlserver/2004/07/showplan";max(//p:RelOp/@Parallel)', 'float') > 0 THEN 'Y' ELSE 'N' END AS is_parallel
	, execution_count
	, CAST((CAST(total_elapsed_time AS FLOAT) / SUM (total_elapsed_time) OVER (PARTITION BY NULL)) * 100.0 AS DECIMAL(5,2)) AS execution_percent
	, SUBSTRING(qt.text,qs.statement_start_offset/2
    , (case when qs.statement_end_offset = -1 
			then len(convert(nvarchar(max), qt.text)) * 2 
			else qs.statement_end_offset end -qs.statement_start_offset)/2
	  ) as query_text
	, db_name(qt.dbid) as database_name
    , CAST(qp.query_plan AS XML) query_plan
	, qt.text AS full_statement
from
(
select 
	(e2.total_elapsed_time - ISNULL(e1.total_elapsed_time ,0)) as total_elapsed_time
	,(e2.total_worker_time - ISNULL(e1.total_worker_time ,0)) as total_worker_time
	,(e2.execution_count - ISNULL(e1.execution_count ,0)) as execution_count
	,e2.statement_start_offset
    ,e2.statement_end_offset
    ,e2.sql_handle sql_handle2
    ,e1.sql_handle sql_handle1
    ,e2.plan_handle plan_handle2
    ,e1.plan_handle plan_handle1
		,e1.query_hash
from #__ExecQStats_1 e1 right outer join #__ExecQStats_2 e2 
	ON e1.plan_handle=e2.plan_handle AND e1.sql_handle = e2.sql_handle 
AND e1.statement_end_offset=e2.statement_end_offset AND e1.statement_start_offset=e2.statement_start_offset 
) as qs
cross apply sys.dm_exec_sql_text(qs.sql_handle2) as qt
cross apply sys.dm_exec_text_query_plan(qs.plan_handle2,statement_start_offset, statement_end_offset) AS qp
WHERE execution_count > 0
--order by CAST(qs.total_elapsed_time / 1000 as FLOAT) / CAST(qs.execution_count AS FLOAT)  desc
ORDER BY qs.total_elapsed_time DESC

IF OBJECT_ID('tempdb.dbo.#__ExecQStats_1') IS NOT NULL DROP TABLE #__ExecQStats_1
IF OBJECT_ID('tempdb.dbo.#__ExecQStats_2') IS NOT NULL DROP TABLE #__ExecQStats_2
Go
EXEC sp_ms_marksystemobject 'dbo.sp_query_diagnose' 