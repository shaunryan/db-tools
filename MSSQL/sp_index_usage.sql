USE [master]
GO
IF OBJECT_ID('sp_index_usage') IS NULL
	EXEC sp_executesql N'CREATE PROCEDURE sp_index_usage AS SELECT 42'
GO
ALTER PROCEDURE sp_index_usage
AS
/*
	Author: Thomas Kejser
	Date: 2014-04-24
	Description: Queries each index for its usage

*/
SELECT
	  T.name AS table_name
	, ISNULL(I.name, T.name) AS index_name
	, I.type_desc AS index_type
	, IU.user_scans + IU.system_scans AS scans
	, IU.user_seeks + IU.system_seeks AS seeks
	, IU.system_lookups + IU.user_lookups AS lookups
	, (SELECT MAX(v) FROM (VALUES 
			(IU.last_user_scan)
			, (IU.last_user_seek)
			, (IU.last_user_lookup)
			, (IU.last_system_scan)
			, (IU.last_system_seek)
			, (IU.last_system_lookup)
	  ) AS value(v)) AS last_read
	, IU.user_lookups + IU.system_updates AS updates
	, CASE WHEN ISNULL(last_user_update,0) > ISNULL(last_system_update, 0) THEN last_user_update ELSE last_system_update END AS last_updated 
	, (SELECT MAX(v) FROM (VALUES 
			(IU.last_user_scan)
			, (IU.last_user_seek)
			, (IU.last_user_lookup)
			, (IU.last_system_scan)
			, (IU.last_system_seek)
			, (IU.last_system_lookup)
			, (IU.last_user_update)
			, (IU.last_system_update) ) AS value(v)) AS last_accessed
FROM sys.indexes I
INNER JOIN sys.tables T
  ON T.object_id = I.object_id
LEFT JOIN sys.dm_db_index_usage_stats IU
  ON I.index_id = IU.index_id
 AND I.object_id = IU.object_id
ORDER BY (SELECT MAX(v) FROM (VALUES 
			(IU.last_user_scan)
			, (IU.last_user_seek)
			, (IU.last_user_lookup)
			, (IU.last_system_scan)
			, (IU.last_system_seek)
			, (IU.last_system_lookup)
			, (IU.last_user_update)
			, (IU.last_system_update)  ) AS value(v) ) DESC

GO
EXEC sys.sp_MS_marksystemobject 'sp_index_usage'
GO
EXEC sp_index_usage