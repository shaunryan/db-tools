﻿/*
   This file is part of db-tools.

    Foobar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	  
*/
USE [master]
GO
IF OBJECT_ID('dbo.sp_table_space_used') IS NULL
	EXEC sys.sp_executesql N'CREATE PROCEDURE sp_table_space_used AS PRINT ''Foo'''
GO
ALTER PROCEDURE dbo.sp_table_space_used
AS
/*
	Author: Thomas Kejser
	Date: 2014-04-16
	Description: Queries all tables in all databases and lists their row count and total space use
*/

/* Get table space */
CREATE TABLE #spaceused 
(
	database_name SYSNAME
	, schema_name SYSNAME
	, table_name SYSNAME
	, table_type SYSNAME
	, indexes BIGINT
	, table_used_kb BIGINT
	, index_used_kb BIGINT
	, row_count BIGINT
	, partitions BIGINT
)

EXEC sp_MSforeachdb '
	USE [?]; 
	INSERT #spaceused 
	SELECT 
	DB_NAME() AS database_name
    , s.name AS schema_name
    , t.name AS table_name 
	, MAX(CASE WHEN i.type_desc IN (''HEAP'', ''CLUSTERED'') THEN i.type_desc ELSE NULL END) AS table_type
	, COUNT(DISTINCT i.index_id) - 1 AS indexes
    , SUM(CASE WHEN i.type_desc IN (''HEAP'', ''CLUSTERED'') THEN a.used_pages * 8 ELSE 0 END) AS table_used_kb 
    , SUM(CASE WHEN i.type_desc IN (''HEAP'', ''CLUSTERED'') THEN 0 ELSE a.used_pages * 8 END) AS index_used_kb 
    , MAX(p.rows) AS row_count
	, MAX(p.partition_number) AS partitions
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
LEFT OUTER JOIN 
    sys.schemas s ON t.schema_id = s.schema_id
WHERE 
    t.NAME NOT LIKE ''dt%'' 
    AND t.is_ms_shipped = 0
    AND i.OBJECT_ID > 255 
GROUP BY t.name, s.name'

SELECT 
  database_name
  , schema_name + '.' + table_name AS table_name
  , table_type
  , indexes
  , partitions
  , row_count
  , table_used_kb / 1024 AS table_used_mb
  , index_used_kb / 1024 AS index_used_mb
  , (table_used_kb + index_used_kb) / 1024 AS total_used_mb
FROM #spaceused
WHERE database_name NOT IN ('tempdb')
ORDER BY table_used_kb + index_used_kb DESC

DROP TABLE #spaceused 

GO
EXEC sp_MS_marksystemobject 'dbo.sp_table_space_used'
GO
EXEC dbo.sp_table_space_used


