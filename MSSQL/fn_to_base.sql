﻿IF OBJECT_ID('dbo.fn_to_base') IS NOT NULL
  DROP FUNCTION dbo.fn_to_base  
GO
CREATE FUNCTION dbo.fn_to_base  
(  
    @value AS BIGINT,  
    @base AS INT  
) 
RETURNS VARCHAR(MAX) AS BEGIN  
    
  /* Validate INput */
  IF @value < 0 OR @base < 2 OR @base > 36 RETURN NULL;  
  
  DECLARE @characters CHAR(36),  
            @result VARCHAR(MAX);  
  
  /* the encoding string and the default result */ 
  SELECT @characters = '0123456789abcdefghijklmnopqrstuvwxyz',  
          @result = '';  
  
  
    
  /* until the value is completely converted, get the modulus  
      of the value and prepend it to the result string.  then  
      divide the value by the base and truncate the remainder */ 
  WHILE @value > 0  
      SELECT @result = SUBSTRING(@characters, @value % @base + 1, 1) + @result,  
              @value = @value / @base;  
  
    RETURN @result; 
END
GO