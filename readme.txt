db-tools

Copyright (C) 2014  Thomas Kejser

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.




Coding Conventions
----------------------------------------------------------------------
When contributing, please follow these conventions:

- Save files as UTF8 (note that the default is UTF16 in SSSM)
- Configure your editor to insert spaces instead of tabs
- Each "tab" should be set to 2 spaces
